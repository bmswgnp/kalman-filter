#!/usr/bin/env python

import rospy
import tf
import numpy as np
from geometry_msgs.msg import Point as PointMSG

class KalmanFilter:
    def __init__(self, x=0, y=0, var=1):
        rospy.loginfo("Kalman filter is initialized, with initial value = %.2f, %.2f", x, y)
        self.sensor_sub = rospy.Subscriber('sensor', PointMSG, self.sensorCorrection)
        self.position_pub = rospy.Publisher('filtered_position', PointMSG)
        self.br = tf.TransformBroadcaster()
        self.time = rospy.Time.now()
        self.delta_t = 0
        self.state = np.array([[x],[y],[0],[0]],np.float64)        # State
        self.state_prev = self.state
        #self.F = np.array([[1,0,self.delta_t,0],[0,1,0,self.delta_t],[0,0,1,0],[0,0,0,1]],np.float64)
        self.H = np.array([[1, 0, 0, 0], [0, 1, 0, 0]],np.float64)
        self.P = var*np.identity(self.state.size)
        self.Q = 0.25*np.identity(self.state.size)
        self.R = 0.09*np.identity(2)
        self.P_prev = self.P
        self.filtered_position = PointMSG()
        self.setPosition(self.state)
        
    def setPosition(self, state):
        self.filtered_position.x = state[0,0]
        self.filtered_position.y = state[1,0]
        self.filtered_position.z = 0
    
    def predictConstVel(self):
        self.delta_t = (rospy.Time.now() - self.time).to_sec()
        self.time = rospy.Time.now()
        F = np.array([[1,0,self.delta_t,0],[0,1,0,self.delta_t],[0,0,1,0],[0,0,0,1]],np.float64)
        self.state = np.dot(F,self.state_prev)      # update state
        self.P = np.add(np.dot(F,np.dot(self.P_prev,F.T)), self.Q)      # update covariance
        self.state_prev = self.state                # store new value to the previous value
        self.P_prev = self.P                        # store new value to the previous value
        self.br.sendTransform((self.state[0,0],self.state[1,0],0),
                              tf.transformations.quaternion_from_euler(0,0,0),
                              rospy.Time.now(),
                              '/estimated',
                              '/world')
        self.setPosition(self.state)
        self.position_pub.publish(self.filtered_position)
        #rospy.loginfo("x = %.2f, y = %.2f", self.state[0,0], self.state[1,0])
        #print self.P
    
    def sensorCorrection(self, reading):
        z = np.array([[reading.x],[reading.y]])
        self.br.sendTransform((reading.x,reading.y,0),
                              tf.transformations.quaternion_from_euler(0,0,0),
                              rospy.Time.now(),
                              '/sensor',
                              '/world')
        K = np.dot(self.P_prev, np.dot(self.H.T,np.linalg.inv(np.add(np.dot(self.H,np.dot(self.P_prev,self.H.T)),self.R))))
        self.state = np.add(self.state_prev, np.dot(K,np.subtract(z,np.dot(self.H,self.state_prev))))
        self.P = np.subtract(self.P_prev,np.dot(K,np.dot(self.H,self.P_prev)))
        self.state_prev = self.state                # store new value to the previous value
        self.P_prev = self.P                        # store new value to the previous value
        #rospy.loginfo("x = %.2f, y = %.2f, vx = %.2f, vy = %.2f", self.state[0,0], self.state[1,0], self.state[2,0], self.state[3,0])
        #print self.P
    
if __name__ == "__main__":
    rospy.init_node('KalmanFilterNode')
    kalman = KalmanFilter()
    r = rospy.Rate(10)
    while not rospy.is_shutdown():
        kalman.predictConstVel()
        r.sleep()
        
