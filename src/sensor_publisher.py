#!/usr/bin/env python

import rospy
import numpy as np
from math import pi as PI
import math
from geometry_msgs.msg import Point as PointMSG

def talker():
    pub = rospy.Publisher('sensor', PointMSG)
    point = PointMSG()
    rospy.init_node('talker')
    r = rospy.Rate(10)
    start = rospy.Time.now()
    while not rospy.is_shutdown():
        t = (rospy.Time.now() - start).to_sec()
        point.x = 5*math.cos(0.25*PI*t) + 0.3*np.random.standard_normal(1)
        point.y = 5*math.sin(0.25*PI*t) + 0.3*np.random.standard_normal(1)
        point.z = 0
        pub.publish(point)
        r.sleep()


if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
